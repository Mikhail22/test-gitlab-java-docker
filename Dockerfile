FROM anapsix/alpine-java:8u202b08_server-jre

RUN apk add jq curl

ENV APM_AGENT_URL="https://search.maven.org/remotecontent?filepath=co/elastic/apm/elastic-apm-agent/1.12.0/elastic-apm-agent-1.12.0.jar"

RUN curl -o elastic-apm-agent.jar ${APM_AGENT_URL}